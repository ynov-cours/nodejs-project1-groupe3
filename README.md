# Contexte 

N'ayant pas parfaitement compris l'énnoncé et les objectifs du projet, nous ne savions pas vraiment quoi faire ni comment le faire et surtout comment se répartir ce travail à 4 et travailler efficacement en groupe sur un projet si court. Nous avons donc choisi de "conceptualiser" tous ensemble, et de partir essayer des méthodes différentes chacun de notre côté. Certains d'entre nous connaissent déjà les bases de Node et d'Express, alors que d'autres pas du tout, et on a donc préféré essayer chacun de notre côté à notre manière (tout en nous aiguillant/aidant si besoin). La branch Master que vous voyez donc n'est qu'une des différentes versions, les autres sont sur les différentes branches du repo, et nous vous invitons à aller les voir pour avoir une idée de nos pistes de recherche (sachant qu'aucun n'est réellement abouti).

## Pour les projet avec node (Branches corentin, enzo & camil)

#### Installer le projet

1. Cloner le projet :
```bash
git clone -b enzo https://gitlab.com/ynov-cours/nodejs-project1-groupe3.git
git clone -b corentin https://gitlab.com/ynov-cours/nodejs-project1-groupe3.git
git clone -b camil https://gitlab.com/ynov-cours/nodejs-project1-groupe3.git
```

2. Installer les dépendances :
```bash
npm i
```

3. Lancer le projet :
```bash
node ./js/main.js (branche enzo)
node . (camil / corentin)
```

Cordialement,
La Team 3
