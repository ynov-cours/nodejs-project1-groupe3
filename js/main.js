//Déclaration des variables pour récupérer les éléments
let src = document.querySelector("#source");
let player = document.querySelector("#player");
let input = document.querySelector("#input");

//Fonction de lecture de vidéo
function readVideo(event) {

    //Log de test pour afficher le fichier
    console.log(event.target.files)

    if (event.target.files && event.target.files[0]) {

        //Utilisation de FileReader API
        let reader = new FileReader();

        //Quand le fichier est chargé
        reader.onload = function(e) {
            console.log('loaded')

            //On lit le fichier en tant qu'URL de vidéo
            src.src = e.target.result

            //On lance le player
            player.load()
        }.bind(this)

        reader.readAsDataURL(event.target.files[0]);
    }
}

//Lancement de la fonction quand on change le contenu de l'input
input.addEventListener('change', readVideo)